let base = 100;
const add = (x) => base + x;
const multiply = (x) => base * x;

const getBase = () => base;

console.log(add(4));
console.log(getBase());
