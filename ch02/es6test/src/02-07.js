let arr = [10, 20, 30, 40];
let [a1, a2, a3] = arr;
console.log(a1, a2, a3);

let p1 = { name: "A", age: 20, gender: "M" };
let { age: a, name: b, gender } = p1;
console.log(a, b, gender);
