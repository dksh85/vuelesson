"use strict";

var arr = [10, 20, 30, 40];
var a1 = arr[0],
  a2 = arr[1],
  a3 = arr[2];
console.log(a1, a2, a3);
var p1 = {
  name: "A",
  age: 20,
  gender: "M"
};
var a = p1.age,
  b = p1.name,
  gender = p1.gender;
console.log(a, b, gender);