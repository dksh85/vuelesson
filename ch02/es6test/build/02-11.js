"use strict";

var obj = {
  result: 0
};
obj.add = function (x, y) {
  this.result = x + y;
};
var add2 = obj.add;
console.log(add2 === obj.add); //type 까지 체크 후 비교
add2(3, 4);
console.log(obj);
console.log(result);