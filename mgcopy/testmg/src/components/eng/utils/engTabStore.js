// tabStore.js
import {onMounted, reactive} from 'vue'
import router from "@/router/index.js"

export const engTabStore = reactive({
    tabs: localStorage.getItem('engTabs') ? JSON.parse(localStorage.getItem('engTabs')) : [], // 탭 정보를 저장할 배열
    currentTab: null
})

export const initTabStore = () => {
    engTabStore.tabs = [];
    engTabStore.currentTab = null;
    localStorage.removeItem('engTabs'); // 로컬 스토리지에서도 제거
}

export function openEngTab(tabInfo) {

    const existingTab = engTabStore.tabs.find(tab => tab.path === tabInfo.path)
    if (!existingTab) {
        engTabStore.tabs.push(tabInfo)
    }

    localStorage.setItem('engTabs', JSON.stringify(engTabStore.tabs))
}

export function closeEngTab(isAll, closingTabPath) {

    let activeTabPath = null
    if ( isAll ) {

        engTabStore.tabs.splice(0, engTabStore.tabs.length)
        localStorage.removeItem('engTabs')
    }
    else {
        let currentTabIndex = engTabStore.tabs.findIndex(tab => tab.path === engTabStore.currentTab.path)
        let closingTabIndex = engTabStore.tabs.findIndex(tab => tab.path === closingTabPath)
        let activeTabIndex = currentTabIndex < closingTabIndex ? currentTabIndex : currentTabIndex-1

        engTabStore.tabs = engTabStore.tabs.filter(tab => tab.path !== closingTabPath)
        if ( engTabStore.tabs.length > 0 ) {
            if ( activeTabIndex < 0 ) {
                activeTabIndex = 0
            }
            activeTabPath = engTabStore.tabs[activeTabIndex].path
        }
        localStorage.setItem('engTabs', JSON.stringify(engTabStore.tabs))
    }

    if ( activeTabPath !== null ) {
        router.push(activeTabPath)
    }
    else {
        router.push({
            name: 'EngHome',
            params: Number(engTabStore.currentTab.params.projectId)
        })
    }
}


