import {
  createRouter,
  createWebHistory,
  createMemoryHistory,
} from "vue-router";

const routes = [
  {
    path: "/",
    name: "EngDefaultLayout",
    component: () => import("../components/DefaultLayout.vue"),
    children: [
      {
        path: "home/",
        name: "EngHome",
        component: () => import("../components/eng/AppHome.vue"),
      },
      {
        path: "members/",
        name: "Members",
        component: () => import("../components/Members.vue"),
      },
    ],
  },
];

const router = createRouter({
  // history: createWebHistory(),
  history: createWebHistory(process.env.NODE_ENV === "production" ? "/" : "/"),
  routes,
});

export default router;
