import { reactive, onMounted, provide, inject } from 'vue'
import axios from '@/utils/axios.js'

const DictionarySymbol = Symbol('DictionarySymbol')

export function provideDictionary() {
  const state = reactive({
    dictionary: new Map(),
    lang: localStorage.getItem('lang'),
    isLoading: false,
    error: null
  })

  async function fetchDictionary() {
    if (state.dictionary.size === 0) {  // 이미 데이터가 로드된 경우 로드하지 않음
      state.isLoading = true
      state.error = null
      try {
        const response = await axios.get('/common/cd-nm?whereCond=')
        const data = response.data.item
        const dict = new Map()
        data.forEach(item => {
          if (!dict.has(item.cultureNm)) {
            dict.set(item.cultureNm, new Map())
          }
          dict.get(item.cultureNm).set(item.commCdId, { name: item.cdNm, desc: item.description })
        })
        state.dictionary = dict
      } catch (error) {
        state.error = error
      } finally {
        state.isLoading = false
      }
    }
  }

  function translate(key) {
    try {
      const map = state.dictionary.get(state.lang)
      if ( map && map.has(key) ) {
        return map.get(key).name
      }
    } catch (error) {
      console.log(error)
    }

    return null
  }

  function description(key) {
    try {
      const map = state.dictionary.get(state.lang)
      if ( map && map.has(key) ) {
        return map.get(key).desc
      }
    } catch (error) {
      console.log(error)
    }

    return null
  }

  function setLang(lang) {
    if ( lang ) {
      state.lang = lang
    }
  }

  function getLang() {
    return state.lang
  }

  onMounted(fetchDictionary)
  provide(DictionarySymbol, { translate, description, setLang, getLang })
}

export function useDictionary() {
  const dictModule = inject(DictionarySymbol)
  if ( !dictModule ) {
    throw new Error('useDictionary() is used without providing data')
  }
  return dictModule
}