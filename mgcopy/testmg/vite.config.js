import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import path from "path";

const base = process.env.NODE_ENV === "production" ? "/" : "/";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue({
      template: {
        compilerOptions: {
          isCustomElement: (tag) => tag.startsWith("swiper-"),
        },
      },
    }),
  ],
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "src"),
    },
  },
  server: {
    host: true,
    port: 5173,
  },
  base: base,
  build: {
    // outDir: 'C:\\Program Files\\Apache Software Foundation\\Tomcat 10.1\\webapps\\kgmg\\static\\',
    emptyOutDir: true,
    minify: "terser",
  },
});
