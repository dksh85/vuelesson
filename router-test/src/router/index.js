import { createRouter, createWebHistory} from 'vue-router'

import Home from '@/pages/Home.vue'
import About from '@/pages/About.vue'
import Members from '@/pages/Members.vue'
import MemberInfo from '@/pages/MemberInfo.vue'
import Videos from '@/pages/Videos.vue'
import Test from '@/pages/Test.vue'
import TroisJSTest from '@/pages/TroisJSTest.vue'
import Demo3 from '@/pages/Demo3.vue'
import VideoPlayer from '@/pages/VideoPlayer.vue'

const router = createRouter({
    history : createWebHistory(),
    routes : [
        { path: '/', name:'home', component: Home },
        { path: '/about', name: 'about', component: About },
        { path: '/members', name: 'members', component: Members },
        { path: '/members/:id', name: 'members/id', component: MemberInfo },
        { path: '/test', component: Test},
        { path: '/troisJSTest', component: TroisJSTest},
        { path: '/demo3', component: Demo3},
        { path: '/videos', name : 'videos', component: Videos, 
          children : [
            {path : ':id', name : 'videos/id', component: VideoPlayer }
          ]
        },
    ]
})

export default router;