import axios from 'axios'


const instance = axios.create({
    baseURL: localStorage.getItem('host'),
    timeout: 30000 // ms.
});

export default instance;